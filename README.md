vault Platform
=============

Description
-----------

[vault](https://www.hashicorp.com/vault.html) is a tool for managing secrets.

Requirements
------------

### Cookbooks and gems

Declared in [metadata.rb](metadata.rb) and in [Gemfile](Gemfile).

### Platforms

A *systemd* managed distribution:
- RHEL Family 7, tested on Centos

Note: it should work quite fine on Debian 8 (with some attributes tuning) but
the official docker image does not allow Systemd to work easily, so it could
not be tested.

Usage
-----

### Important Notes

This cookbook will install and configure a vault cluster using
a consul backend.

Installed and started vault need to be initialized first.

You can use the following command to initialize it:

`vault init -key-shares=5 -key-threshold=2`

### Easy Setup

Set `node['vault-platform']['hosts']` to an array containing the
hostnames of the nodes of the vault cluster

### Search

The recommended way to use this cookbook is through the creation of a role per
**vault** cluster. This enables the search by role feature, allowing a
simple service discovery.

In fact, there are two ways to configure the search:

1. With a static configuration through a list of hostnames (attributes `hosts`
   that is `node['vault-platform']['hosts']`) for nodes belonging to
   vault cluster.
2. With a real search, performed on a role (attributes `role` and `size`
   like in `node['vault-platform']['role']`).
   The role should be in the run-list of all nodes of the cluster.
   The size is a safety and should be the number of nodes of this role.

If hosts is configured, `role` and `size` are ignoredi

You also need a **consul** cluster. This is not in the scope of this
cookbook but if you need one, you should consider using [Consul
Platform][consul-platform].

The configuration of Consul hosts use search and is done similarly as for
**vault** hosts, _ie_ with a static list of hostnames or by using a search on
a role. The attribute to configure is `['vault-platform']['consul']`.

See [roles](test/integration/roles) for some examples and
[Cluster Search][cluster-search] documentation for more information.


Attributes
----------

Configuration is done by overriding default attributes. All configuration keys
have a default defined in [attributes/default.rb](attributes/default.rb).
Please read it to have a comprehensive view of what and how you can configure
this cookbook behavior.

Recipes
-------

### default

Include `search`, `user`, `install`, `config` and `systemd` recipes.

### search

Search the node belonging to the vault cluster.

### user

Create user/group used by vault.

### install

Install vault using ark.

### config

Generate options for the vault node.

Global options for each vault node in the cluster are defined through
the following attribute: `node['vault-platform']['options']`.

### systemd

Create systemd service file for vault.

Changelog
---------

Available in [CHANGELOG.md](CHANGELOG.md).

Contributing
------------

Please read carefully [CONTRIBUTING.md](CONTRIBUTING.md) before making a merge
request.

License and Author
------------------

- Author:: Samuel Bernard (<samuel.bernard@s4m.io>)
- Author:: Florian Philippon (<florian.philippon@s4m.io>)

```text
Copyright (c) 2016 Sam4Mobile, Doctolib

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
[cluster-search cookbook]: https://supermarket.chef.io/cookbooks/cluster-search
