#
# Copyright (c) 2015-2016 Sam4Mobile, Doctolib
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

describe 'Vault app' do
  it 'is running' do
    expect(service('vault')).to be_running
  end
  it 'is enabled at boot' do
    expect(service('vault')).to be_running
  end
  it 'is listening for connections' do
    expect(port(8200)).to be_listening
  end
end

describe 'Vault Configuration' do
  describe file('/opt/vault/config.json') do
    its(:content) { should contain 'consul-vault-centos-7:8500' }
    its(:content) { should contain 'disable_mlock' }
  end
end
