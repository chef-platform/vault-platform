#
# Copyright (c) 2016 Sam4Mobile, Doctolib
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Construct command line options
options = node.run_state[cookbook_name]['options'].map do |key, opt|
  "-#{key.to_s.tr('_', '-')}#{" #{opt}" unless opt.nil?}"
end.join(' ')

# Configure systemd unit with options
unit = node[cookbook_name]['systemd_unit'].to_hash
bin = "#{node[cookbook_name]['prefix_home']}/vault/vault"
command = node[cookbook_name]['exec_command']
unit['Service']['ExecStart'] = "#{bin} #{command} #{options}"

systemd_unit 'vault.service' do
  enabled true
  active true
  masked false
  static false
  content unit
  triggers_reload true
  action %i[create enable start]
end
