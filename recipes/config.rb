#
# Copyright (c) 2016 Sam4Mobile, Doctolib
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Deploy vault configuration
config = node[cookbook_name]['config'].to_hash
# Build consul address
config['backend']['consul']['address'] =
  node.run_state.dig(cookbook_name, 'consul_addr')

raise 'No consul found!' if config['backend']['consul']['address'].nil?

file node[cookbook_name]['config_file'] do
  content "#{JSON.pretty_generate(config)}\n"
  owner node[cookbook_name]['user']
  group node[cookbook_name]['group']
  mode '0640'
end

# Store options to reuse them later
node.run_state[cookbook_name]['options'] =
  node[cookbook_name]['options'].to_hash
