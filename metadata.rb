name 'vault-platform'
maintainer 'Sam4Mobile, Doctolib'
maintainer_email 'dps.team@s4m.io'
license 'Apache-2.0'
description 'Cookbook used to install and configure a vault cluster'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://gitlab.com/chef-platform/vault-platform'
issues_url 'https://gitlab.com/chef-platform/vault-paltform/issues'
version '0.9.2'
chef_version 13

supports 'centos', '>= 7.1'

depends 'cluster-search'
depends 'ark'
