# Copyright (c) 2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

cookbook_name = 'vault-platform'

# Vault cluster configuration with cluster-search
# Role used by the search to find other nodes of the cluster
default[cookbook_name]['role'] = cookbook_name
# Hosts of the cluster, deactivate search if not empty
default[cookbook_name]['hosts'] = []
# Expected size of the cluster. Ignored if hosts is not empty
default[cookbook_name]['size'] = 1

# Consul cluster configuration with cluster-search
# Role used by the search to find other nodes of the cluster
default[cookbook_name]['consul']['role'] = 'consul-vault-cluster'
# Hosts of the cluster, deactivate search if not empty
default[cookbook_name]['consul']['hosts'] = []
# Expected size of the cluster. Ignored if hosts is not empty
default[cookbook_name]['consul']['size'] =  3

# Vault version
default[cookbook_name]['version'] = '0.10.2'
version = node[cookbook_name]['version']
# package sha256 checksum
default[cookbook_name]['checksum'] =
  'f725be68316d10ef2e7779fdedd8eb5d4ed9975303c828466bb9a729e01392dd'

# Where to get the zip file
binary = "vault_#{version}_linux_amd64.zip"
default[cookbook_name]['mirror'] =
  "https://releases.hashicorp.com/vault/#{version}/#{binary}"

# User and group of Vault process
default[cookbook_name]['user'] = 'vault'
default[cookbook_name]['group'] = 'vault'

# Where to put installation dir
default[cookbook_name]['prefix_root'] = '/opt'
# Where to link installation dir
default[cookbook_name]['prefix_home'] = '/opt'
# Where to link binaries
default[cookbook_name]['prefix_bin'] = '/opt/bin'

# Vault config file location
default[cookbook_name]['config_file'] =
  "#{node[cookbook_name]['prefix_root']}/vault/config.json"

# Vault daemon options
default[cookbook_name]['options'] = {
  'config' => node[cookbook_name]['config_file']
}

# Vault configuration
default[cookbook_name]['config'] = {
  'listener' => {
    'tcp' => {
      'address' => ':8200'
    }
  },
  'backend' => {
    'consul' => {
      'cluster_addr' => "http://#{node['fqdn']}:8200"
    }
  }
}

# Vault exec command
default[cookbook_name]['exec_command'] = 'server'

# Systemd service unit, include config
default[cookbook_name]['systemd_unit'] = {
  'Unit' => {
    'Description' => 'Vault agent',
    'After' => 'network.target'
  },
  'Service' => {
    'Type' => 'simple',
    'User' => node[cookbook_name]['user'],
    'Group' => node[cookbook_name]['group'],
    'Restart' => 'on-failure',
    'ExecStart' => 'TO_BE_COMPLETED'
  },
  'Install' => {
    'WantedBy' => 'multi-user.target'
  }
}
